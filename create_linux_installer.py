#!/usr/bin/python

import glob
import imp
import os
import re
import shutil
import subprocess
import sys
import tarfile
import time
import yapm
import zipfile

class InstallerConfiguration:
    def __init__(self, arguments):
        """(self, list of strings) -> NoneType"""
        if len(sys.argv) < 7:
            raise Exception('Parameter missing')
        self.installerBaseDir = os.path.join(arguments[1], 'linux_installer')
        self.cmakeBinaryDir = arguments[2]
        self.installerName = arguments[3]
        self.architecture = arguments[4]
        self.buildType = arguments[5]
        self.time_stamp = time.strftime("-%Y%m%d")
        i = 6
        self.binaries = []
        while i < len(sys.argv):
            self.binaries.append(arguments[i])
            i+=1
        self.validate()

    def __str__(self):
        """(self) -> string"""
        retStr = ''
        retStr += 'Installer base directory: ' + self.installerBaseDir
        retStr += '\n'
        retStr += 'CMake binary directory:   ' + self.cmakeBinaryDir
        retStr += '\n'
        retStr += 'Installer name:           ' + self.installerName
        retStr += '\n'
        retStr += 'Architecture:             ' + self.architecture
        retStr += '\n'
        retStr += 'Library directory:        ' + self.libDirectory
        retStr += '\n'
        retStr += 'Installer suffix:         ' + self.installerSuffix
        retStr += '\n'
        retStr += 'Build type:               ' + self.buildType
        retStr += '\n'
        retStr += 'Binaries:                 ' + str(self.binaries)
        return retStr

    def validate(self):
        self.validateArchitecture()
        self.validateBuildType()
        self.validateBinariesExists()
        self.validateLibDirExists()
        self.validateMojoSetupDirExists()

    def validateArchitecture(self):
        if self.architecture == 'LINUX_X86_32':
            self.libDirectory = 'lib32'
            self.installerSuffix = 'x86'
        elif self.architecture == 'LINUX_X86_64':
            self.libDirectory = 'lib64'
            self.installerSuffix = 'x86_64'
        elif self.architecture == 'LINUX_PPC':
            self.libDirectory = 'libppc'
            self.installerSuffix = 'ppc'
        else:
            raise Exception('Unsupported architecture: %s' % architecture)

    def validateBuildType(self):
        if self.buildType != 'RELEASE' and self.buildType != 'DEMO':
            raise Exception('Unsupported build type: %s' % self.buildType)

    def validateBinariesExists(self):
        for binary in self.binaries:
            if not os.path.isfile(os.path.join(self.cmakeBinaryDir, binary)):
                raise Exception('Binary %s does not exit in %s.' % (binary, self.cmakeBinaryDir))

    def validateLibDirExists(self):
        if not os.path.isdir(os.path.join(self.cmakeBinaryDir, self.libDirectory)):
            raise Exception('Library directory %s not found in %s.' % (self.libDirectory, self.cmakeBinaryDir))

    def validateMojoSetupDirExists(self):
        if not os.path.isdir(os.path.join(self.installerBaseDir, 'mojosetup')):
            raise Exception('MojoSetup not found in %s.' % (self.installerBaseDir))

    def getCMakeBinaryDir(self):
        return self.cmakeBinaryDir

    def getBinaryName(self):
        return self.binaryName

    def getInstallerOutputDirectory(self):
        return os.path.join(self.cmakeBinaryDir, 'linux_installer_' + self.installerName + self.time_stamp)

    def getAbsoluteBinaryName(self, binaryName):
        return os.path.join(self.cmakeBinaryDir, binaryName)

    def getAbsoluteBinaryBackupName(self, binaryName):
        return os.path.join(self.getInstallerOutputDirectory(), binaryName + '-' + self.getInstallerSuffix())

    def getAbsoluteLibraryDirectory(self):
        return os.path.join(self.cmakeBinaryDir, self.libDirectory)

    def getLibraryDirectory(self):
        return self.libDirectory

    def getInstallerBaseDirectory(self):
        return self.installerBaseDir

    def getArchitecture(self):
        return self.architecture

    def doesInstallerFileMatchBuildType(self, file):
        # return True if the build type matches the installer file, otherwise False is returned
        return (self.buildType == 'RELEASE' and not file.find('demo') != -1) or (self.buildType == 'DEMO' and file.find('demo') != -1)

    def getInstallerSuffix(self):
        return self.installerSuffix + self.time_stamp

def prepareBinaries(installerConfig):
    # create installer output and 'installer_bins' temporary directory
    binariesDir = os.path.join(installerConfig.getInstallerOutputDirectory(), 'installer_bins')
    try:
        os.makedirs(binariesDir)
    except OSError as e:
        print(e)
        pass

    # backup original executables, there is no need to backup the libraries too, because they are under version control
    for binary in installerConfig.binaries:
        shutil.copyfile(installerConfig.getAbsoluteBinaryName(binary), installerConfig.getAbsoluteBinaryBackupName(binary))
        os.chmod(installerConfig.getAbsoluteBinaryBackupName(binary), 0755)
        # copy the binary which will be stripped in one of the next steps
        shutil.copy(installerConfig.getAbsoluteBinaryName(binary), binariesDir)

    # copy all libraries which will be stripped in the next step
    yapm.copyDir(installerConfig.getAbsoluteLibraryDirectory(), os.path.join(binariesDir, installerConfig.getLibraryDirectory()))

    # remove unneeded libaries
    unneeded_libs_pattern = ['libboost_unit_test_framework*', 'libboost_prg_exec_monitor*']
    for unneeded_lib_pattern in unneeded_libs_pattern:
        unneeded_libs = glob.glob(os.path.join(binariesDir, installerConfig.getLibraryDirectory(), unneeded_lib_pattern))
        for unneeded_lib in unneeded_libs:
            os.remove(unneeded_lib)

    # strip the remaining libraries
    for root, dirs, files in os.walk(binariesDir):
        for file in files:
            if not file.endswith('.txt'):
                subprocess.Popen(['strip', '--strip-debug', '--strip-unneeded', os.path.join(root, file)], cwd=binariesDir).communicate()
    return binariesDir

def isFullInstaller(scriptName):
    # a full installer contains all the needed data files and does not rely on data files from a CDROM or a DVD
    return (scriptName.find('cdrom') == -1) and (scriptName.find('dvd') == -1)

def getInstallerName(scriptName):
    return scriptName[:-3]

def calcSha256Sum(fileName, filePath):
    checksum = subprocess.Popen(['sha256sum', fileName], cwd=filePath, stdout=subprocess.PIPE).communicate()[0]
    checksumFile = open(os.path.join(filePath, fileName + '.sha256'), 'w')
    checksumFile.write(checksum)
    checksumFile.close

def filter_function(tarinfo):
    if re.match('language_\D{2}.ini', tarinfo.name) == None:
        return tarinfo
    else:
        return None

if __name__ == '__main__':
    installerConfiguration = InstallerConfiguration(sys.argv)
    print(installerConfiguration)
    binariesDir = prepareBinaries(installerConfiguration)
    cwd = os.path.dirname(os.path.realpath(__file__))
    os.chdir(installerConfiguration.getInstallerBaseDirectory())
    for file in glob.glob('*.py'):
        if not installerConfiguration.doesInstallerFileMatchBuildType(file):
            continue
        # create temporary installer directory
        installerName = getInstallerName(file) + '_' + installerConfiguration.getInstallerSuffix()
        tmpInstallerDir = os.path.join(installerConfiguration.getInstallerOutputDirectory(), installerName)
        try:
            os.mkdir(tmpInstallerDir)
        except OSError as e:
            print(e)
            pass

        installerFileName = installerName + '.mojo.run'
        # copy content of mojosetup git submodule directory to the temporary installer directory,
        # skip the'.git' directory and
        # ignore LIFLG specific files and unused binaries
        yapm.copyDir(os.path.join(installerConfiguration.getInstallerBaseDirectory(), 'mojosetup', 'guis'), os.path.join(tmpInstallerDir, 'guis'))
        yapm.copyDir(os.path.join(installerConfiguration.getInstallerBaseDirectory(), 'mojosetup', 'meta'), os.path.join(tmpInstallerDir, 'meta'))
        yapm.copyDir(os.path.join(installerConfiguration.getInstallerBaseDirectory(), 'mojosetup', 'scripts'), os.path.join(tmpInstallerDir, 'scripts'))
        # copy content of installer directory to temporary installer directory
        yapm.copyDir(os.path.join(installerConfiguration.getInstallerBaseDirectory(), getInstallerName(file)), tmpInstallerDir)
        # copy binary and libraries to temporary installer directory
        try:
            os.mkdir(os.path.join(tmpInstallerDir, 'data'))
        except OSError:
            pass
        # copy binary and libraries to temporary installer directory
        yapm.copyDir(binariesDir, os.path.join(tmpInstallerDir, 'data'))

        # run installer specific script
        py_mod = imp.load_source('', file)
        py_mod.installer_specific_tasks(tmpInstallerDir, cwd, installerConfiguration.getLibraryDirectory())

        # tar all files in the 'data' subdirectory
        os.rename(os.path.join(tmpInstallerDir, 'data'), os.path.join(tmpInstallerDir, installerName))
        try:
            os.mkdir(os.path.join(tmpInstallerDir, 'data'))
        except OSError:
            pass

        if isFullInstaller(getInstallerName(file)):
            tar = tarfile.open(os.path.join(installerConfiguration.getInstallerOutputDirectory(), installerName + '.tar'), 'w')
            tar.add(os.path.join(tmpInstallerDir, installerName), arcname=installerName)
            tar.close()

        tar = tarfile.open(os.path.join(tmpInstallerDir, 'data/data.tar'), 'w')
        tar.add(os.path.join(tmpInstallerDir, installerName), arcname='', filter=filter_function)
        tar.close()

        if os.path.isfile(os.path.join(tmpInstallerDir, installerName, 'README')):
            shutil.copy(os.path.join(tmpInstallerDir, installerName, 'README'), os.path.join(tmpInstallerDir, 'data'))

        ini_files = glob.iglob(os.path.join(tmpInstallerDir, installerName, "language_*.ini"))
        for ini_file in ini_files:
            shutil.copy(os.path.join(tmpInstallerDir, installerName, ini_file), os.path.join(tmpInstallerDir, 'data'))

        shutil.rmtree(os.path.join(tmpInstallerDir, installerName))

        # zip temporary installer directory and glue zip together with mojosetup binary
        if installerConfiguration.getArchitecture() == 'LINUX_X86_32':
            mojo = os.path.join(installerConfiguration.getInstallerBaseDirectory(), 'mojosetup', 'mojosetup.x86')
            globPattern = '*.x86.so'
        elif installerConfiguration.getArchitecture() == 'LINUX_X86_64':
            mojo = os.path.join(installerConfiguration.getInstallerBaseDirectory(), 'mojosetup', 'mojosetup.x86_64')
            globPattern = '*.x86_64.so'
        else:
            raise Exception('No MojoSetup binary available for %s.' % (installerConfiguration.installerBaseDir))

        for filename in (set(glob.glob(os.path.join(tmpInstallerDir, 'guis', '*.*'))) - set(glob.glob(os.path.join(tmpInstallerDir, 'guis', globPattern)))):
            os.remove(os.path.join(tmpInstallerDir, 'guis', filename))

        zip = zipfile.ZipFile(tmpInstallerDir + '.zip', 'w', compression=zipfile.ZIP_DEFLATED)
        root_len = len(os.path.abspath(tmpInstallerDir))
        for root, dirs, files in os.walk(tmpInstallerDir):
            archive_root = os.path.abspath(root)[root_len:]
            for f in files:
                fullpath = os.path.join(root, f)
                archive_name = os.path.join(archive_root, f)
                zip.write(fullpath, archive_name, zipfile.ZIP_DEFLATED)
        zip.close()

        destination = open(os.path.join(installerConfiguration.getInstallerOutputDirectory(), installerFileName),'wb')
        shutil.copyfileobj(open(mojo,'rb'), destination)
        shutil.copyfileobj(open(tmpInstallerDir + '.zip','rb'), destination)
        destination.close()

        # chmod 755
        os.chmod(os.path.join(installerConfiguration.getInstallerOutputDirectory(), installerFileName), 0755)

        # remove zip file
        os.remove(tmpInstallerDir + '.zip')

        # clean up - remove temporary installer directory
        shutil.rmtree(tmpInstallerDir)

        # calculate sha256 hash
        calcSha256Sum(installerFileName, installerConfiguration.getInstallerOutputDirectory());
        if isFullInstaller(getInstallerName(file)):
            print('Compressing tarball installer')
            subprocess.Popen(['xz', '--verbose', os.path.join(installerConfiguration.getInstallerOutputDirectory(), installerName + '.tar')], cwd=installerConfiguration.getInstallerOutputDirectory()).communicate()
            calcSha256Sum(installerName + '.tar.xz', installerConfiguration.getInstallerOutputDirectory());

    # clean up - delete stripped binary and libraries
    shutil.rmtree(binariesDir)
