#Allow easy usage of the IWYU tool (http://include-what-you-use.org/)
option(ENABLE_IWYU "Enable include-what-you-use" OFF)
if(ENABLE_IWYU)
    find_program(IWYU_PATH NAMES include-what-you-use iwyu)
    if(NOT IWYU_PATH)
        message(FATAL_ERROR "Could not find the program include-what-you-use")
    endif()
    set(CMAKE_CXX_INCLUDE_WHAT_YOU_USE
        ${IWYU_PATH}
        -Xiwyu --no_comments # Requires at least version 0.6
    )
    set(CMAKE_C_INCLUDE_WHAT_YOU_USE ${IWYU_PATH})
endif()
