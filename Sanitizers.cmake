cmake_minimum_required(VERSION 3.1)

#Import command cmake_dependent_option
include(CMakeDependentOption)

#Allow user to enable/disable and configure sanitizers
#More about sanitizers:
# - AddressSanitizer: http://clang.llvm.org/docs/AddressSanitizer.html
# - ThreadSanitizer: http://clang.llvm.org/docs/ThreadSanitizer.html
# - MemorySanitizer: http://clang.llvm.org/docs/MemorySanitizer.html
# - UBSanitizer: http://clang.llvm.org/docs/UndefinedBehaviorSanitizer.html
option(ENABLE_SANITIZER "Enable one or more sanitizers for GCC/Clang" OFF)
cmake_dependent_option(ENABLE_SANITIZER_ADDRESS "Address sanitizer" OFF "ENABLE_SANITIZER" OFF)
cmake_dependent_option(ENABLE_SANITIZER_UNDEFINED_BEHAVIOUR "Undefined behaviour sanitizer" OFF "ENABLE_SANITIZER" OFF)
cmake_dependent_option(ENABLE_SANITIZER_THREAD "Thread sanitizer (Clang, Linux, x86_64 only)" OFF
  "ENABLE_SANITIZER;NOT ENABLE_SANITIZER_ADDRESS;NOT ENABLE_SANITIZER_MEMORY;NOT ENABLE_GCOV" OFF)
if("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
  cmake_dependent_option(ENABLE_SANITIZER_MEMORY "Enable the memory sanitizer (Clang, Linux, x86_64 only)" ON "ENABLE_SANITIZER;NOT ENABLE_SANITIZER_ADDRESS" OFF)
endif()

if(ENABLE_SANITIZER_ADDRESS)
  add_compile_options(-fsanitize=address -fno-omit-frame-pointer)
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fsanitize=address -pthread")
endif()
if(ENABLE_SANITIZER_MEMORY)
  add_compile_options(-fsanitize=memory -fno-omit-frame-pointer)
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fsanitize=memory")
endif()
if(ENABLE_SANITIZER_THREAD)
  add_compile_options(-fsanitize=thread -fPIC)
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fsanitize=thread")
endif()
if(ENABLE_SANITIZER_UNDEFINED_BEHAVIOUR)
  add_compile_options(-fsanitize=undefined -fno-omit-frame-pointer)
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fsanitize=undefined")
endif()
