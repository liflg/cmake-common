function(add_installer_target INSTALLER_NAME INSTALLER_BASE_DIRECTORY)
    find_package(PythonInterp 2)
    if(${PYTHONINTERP_FOUND})
        add_custom_target(installer
            COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_COMMON_BASE_DIRECTORY}/create_linux_installer.py ${INSTALLER_BASE_DIRECTORY} ${CMAKE_BINARY_DIR} ${INSTALLER_NAME} ${TARGET_ARCHITECTURE} RELEASE ${ARGN}
            DEPENDS ${ARGN}
        )
    else()
        message(WARNING "installer target not available, because of missing Python interpreter")
    endif()
endfunction(add_installer_target)

function(add_demo_installer_target INSTALLER_NAME DEMO_BASE_DIRECTORY)
    find_package(PythonInterp 2)
    if(${PYTHONINTERP_FOUND})
        add_custom_target(demo
            COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_COMMON_BASE_DIRECTORY}/create_linux_installer.py ${DEMO_BASE_DIRECTORY} ${CMAKE_BINARY_DIR} ${INSTALLER_NAME} ${TARGET_ARCHITECTURE} DEMO ${ARGN}
            DEPENDS ${ARGN}
        )
    else()
        message(WARNING "demo_installer target not available, because of missing Python interpreter")
    endif()
endfunction(add_demo_installer_target)
