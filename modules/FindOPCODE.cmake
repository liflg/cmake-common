# - Try to find the library OPCODE (http://www.codercorner.com/Opcode.htm)
# Once done this will define
#  OPCODE_FOUND - System has OPCODE
#  OPCODE_INCLUDE_DIRS - The OPCODE include directories
#  OPCODE_LIBRARIES - The libraries needed to use OPCODE

find_path(OPCODE_INCLUDE_DIR Opcode.h
          PATH_SUFFIXES OPCODE-1.2 )

find_library(OPCODE_LIBRARY NAMES OPCODE libOPCODE)

set(OPCODE_LIBRARIES ${OPCODE_LIBRARY} )
set(OPCODE_INCLUDE_DIRS ${OPCODE_INCLUDE_DIR} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set OPCODE_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(OPCODE  DEFAULT_MSG
                                  OPCODE_LIBRARY OPCODE_INCLUDE_DIR)

mark_as_advanced(OPCODE_INCLUDE_DIR OPCODE_LIBRARY )
