# Try to find OpusFile
# Once done this will define
# OPUSFILE_FOUND - System has OpusFile
# OPUSFILE_INCLUDE_DIRS - The OpusFile include directories
# OPUSFILE_LIBRARIES - The libraries needed to use OpusFile

find_path(OPUSFILE_INCLUDE_DIR opusfile.h PATH_SUFFIXES opus)
find_library(OPUSFILE_LIBRARY NAMES opusfile)

set(OPUSFILE_INCLUDE_DIRS ${OPUSFILE_INCLUDE_DIR})
set(OPUSFILE_LIBRARIES ${OPUSFILE_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(OPUSFILE DEFAULT_MSG OPUSFILE_INCLUDE_DIR OPUSFILE_LIBRARY)

mark_as_advanced(OPUSFILE_INCLUDE_DIR OPUSFILE_LIBRARY)
