# - Try to find SMPEG2
# Once done this will define
#
#  SMPEG2_FOUND - system has SMPEG2
#  SMPEG2_INCLUDE_DIR - the SMPEG2 include directory
#  SMPEG2_LIBRARY - Link these to use SMPEG2

if (SMPEG2_LIBRARY AND SMPEG2_INCLUDE_DIR)
  # in cache already
  set(SMPEG2_FOUND TRUE)
else (SMPEG2_LIBRARY AND SMPEG2_INCLUDE_DIR)
  find_path(SMPEG2_INCLUDE_DIR
    NAMES
      smpeg.h
    PATHS
      /usr/include
      /usr/local/include
      /opt/local/include
      /sw/include
      $ENV{HOME}/bin/include
      $ENV{HOME}/local/include
      $ENV{HOME}/bin/local/include
    PATH_SUFFIXES
      smpeg2
  )

  find_library(SMPEG2_LIBRARY
    NAMES
      smpeg2
    PATHS
      /usr/lib
      /usr/local/lib
      /opt/local/lib
      /sw/lib
      $ENV{HOME}/bin/lib
      $ENV{HOME}/local/lib
      $ENV{HOME}/bin/local/lib
  )

  if (SMPEG2_INCLUDE_DIR AND SMPEG2_LIBRARY)
     set(SMPEG2_FOUND TRUE)
  endif (SMPEG2_INCLUDE_DIR AND SMPEG2_LIBRARY)

  if (SMPEG2_FOUND)
    if (NOT SMPEG2_FIND_QUIETLY)
      message(STATUS "Found SMPEG2: ${SMPEG2_LIBRARY}")
    endif (NOT SMPEG2_FIND_QUIETLY)
  else (SMPEG2_FOUND)
    if (SMPEG2_FIND_REQUIRED)
      message(FATAL_ERROR "Could not find SMPEG2")
    endif (SMPEG2_FIND_REQUIRED)
  endif (SMPEG2_FOUND)

endif (SMPEG2_LIBRARY AND SMPEG2_INCLUDE_DIR)

