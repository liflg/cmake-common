# Copyright (c) 2009, Whispersoft s.r.l.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
# * Neither the name of Whispersoft s.r.l. nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# Finds Theora library
#
#  THEORA_INCLUDE_DIR - where to find theora.h, etc.
#  THEORA_LIBRARIES   - List of libraries when using Theora.
#  THEORA_FOUND       - True if Theora found.
#
# Changes
# 2012-09-17: Changed variable names to all uppercase (Reto Schneider)

if(THEORA_INCLUDE_DIR)
  # Already in cache, be silent
  set(THEORA_FIND_QUIETLY TRUE)
endif(THEORA_INCLUDE_DIR)

find_path(THEORA_INCLUDE_DIR theora/theora.h
  /opt/local/include
  /usr/local/include
  /usr/include
)

set(THEORA_NAMES theora)
find_library(THEORA_LIBRARY
  NAMES ${THEORA_NAMES}
  PATHS /usr/lib /usr/local/lib /opt/local/lib
)

if(THEORA_INCLUDE_DIR AND THEORA_LIBRARY)
   set(THEORA_FOUND TRUE)
   set(THEORA_LIBRARIES ${THEORA_LIBRARY})
else(THEORA_INCLUDE_DIR AND THEORA_LIBRARY)
   set(THEORA_FOUND FALSE)
   set(THEORA_LIBRARIES)
endif(THEORA_INCLUDE_DIR AND THEORA_LIBRARY)

if(THEORA_FOUND)
   if (NOT THEORA_FIND_QUIETLY)
      message(STATUS "Found Theora: ${THEORA_LIBRARY}")
   endif (NOT THEORA_FIND_QUIETLY)
else(THEORA_FOUND)
   if(THEORA_FIND_REQUIRED)
      message(STATUS "Looked for Theora libraries named ${THEORA_NAMES}.")
      message(STATUS "Include file detected: [${THEORA_INCLUDE_DIR}].")
      message(STATUS "Lib file detected: [${THEORA_LIBRARY}].")
      message(FATAL_ERROR "=========> Could NOT find Theora library")
   endif(THEORA_FIND_REQUIRED)
endif(THEORA_FOUND)
