# our own version of FindFreetype.cmake because of
# http://public.kitware.com/Bug/view.php?id=14601 and
# http://public.kitware.com/Bug/view.php?id=14626

find_path(
        FREETYPE_INCLUDE_DIRS
        ft2build.h
        PATH_SUFFIXES freetype2
)

find_library(
        FREETYPE_LIBRARIES
        freetype
)

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(
        FREETYPE
        DEFAULT_MSG
        FREETYPE_INCLUDE_DIRS
        FREETYPE_LIBRARIES
)
