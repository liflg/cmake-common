# Try to find Opus
# Once done this will define
# OPUS_FOUND - System has Opus
# OPUS_INCLUDE_DIRS - The Opus include directories
# OPUS_LIBRARIES - The libraries needed to use Opus

find_path(OPUS_INCLUDE_DIR opus.h PATH_SUFFIXES opus)
find_library(OPUS_LIBRARY NAMES opus)

set(OPUS_INCLUDE_DIRS ${OPUS_INCLUDE_DIR})
set(OPUS_LIBRARIES ${OPUS_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(OPUS DEFAULT_MSG OPUS_INCLUDE_DIR OPUS_LIBRARY)

mark_as_advanced(OPUS_INCLUDE_DIR OPUS_LIBRARY)
