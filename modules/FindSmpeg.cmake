# - Try to find SMPEG
# Once done this will define
#
#  SMPEG_FOUND - system has SMPEG
#  SMPEG_INCLUDE_DIR - the SMPEG include directory
#  SMPEG_LIBRARY - Link these to use SMPEG

if (SMPEG_LIBRARY AND SMPEG_INCLUDE_DIR)
  # in cache already
  set(SMPEG_FOUND TRUE)
else (SMPEG_LIBRARY AND SMPEG_INCLUDE_DIR)
  find_path(SMPEG_INCLUDE_DIR
    NAMES
      smpeg.h
    PATHS
      /usr/include
      /usr/local/include
      /opt/local/include
      /sw/include
      $ENV{HOME}/bin/include
      $ENV{HOME}/local/include
      $ENV{HOME}/bin/local/include
    PATH_SUFFIXES
      smpeg
  )

  find_library(SMPEG_LIBRARY
    NAMES
      smpeg
    PATHS
      /usr/lib
      /usr/local/lib
      /opt/local/lib
      /sw/lib
      $ENV{HOME}/bin/lib
      $ENV{HOME}/local/lib
      $ENV{HOME}/bin/local/lib
  )

  if (SMPEG_INCLUDE_DIR AND SMPEG_LIBRARY)
     set(SMPEG_FOUND TRUE)
  endif (SMPEG_INCLUDE_DIR AND SMPEG_LIBRARY)

  if (SMPEG_FOUND)
    if (NOT SMPEG_FIND_QUIETLY)
      message(STATUS "Found SMPEG: ${SMPEG_LIBRARY}")
    endif (NOT SMPEG_FIND_QUIETLY)
  else (SMPEG_FOUND)
    if (SMPEG_FIND_REQUIRED)
      message(FATAL_ERROR "Could not find SMPEG")
    endif (SMPEG_FIND_REQUIRED)
  endif (SMPEG_FOUND)

endif (SMPEG_LIBRARY AND SMPEG_INCLUDE_DIR)

