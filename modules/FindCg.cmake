# Try to find the Nvidia Cg Toolkit
# Once done this will define
# CG_FOUND - System has Cg
# CG_INCLUDE_DIRS - The Cg include directories
# CG_LIBRARIES - The libraries needed to use Cg

find_path(CG_INCLUDE_DIR Cg/cg.h)
find_library(CG_LIBRARY NAMES Cg)

find_path(CGGL_INCLUDE_DIR Cg/cgGL.h)
find_library(CGGL_LIBRARY NAMES CgGL)

set(CG_INCLUDE_DIRS ${CG_INCLUDE_DIR} ${CGGL_INCLUDE_DIR})
set(CG_LIBRARIES ${CG_LIBRARY} ${CGGL_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CG DEFAULT_MSG CG_INCLUDE_DIR CG_LIBRARY CGGL_INCLUDE_DIR CGGL_LIBRARY)

mark_as_advanced(CG_INCLUDE_DIR CG_LIBRARY CGGL_INCLUDE_DIR CGGL_LIBRARY)
