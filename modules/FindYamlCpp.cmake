# - Try to find the library YAML Cpp (https://github.com/jbeder/yaml-cpp)
# Once done this will define
#  YAMLCPP_FOUND - System has YAMLCPP
#  YAMLCPP_INCLUDE_DIRS - TheYAMLCPP include directories
#  YAMLCPP_LIBRARIES - The libraries needed to use YAMLCPP

find_path(YAMLCPP_INCLUDE_DIR yaml-cpp/yaml.h)

find_library(YAMLCPP_LIBRARY NAMES yaml-cpp)

set(YAMLCPP_LIBRARIES ${YAMLCPP_LIBRARY})
set(YAMLCPP_INCLUDE_DIRS ${YAMLCPP_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set YAMLCPP_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(YAMLCPP DEFAULT_MSG
                                  YAMLCPP_LIBRARY YAMLCPP_INCLUDE_DIR)

mark_as_advanced(YAMLCPP_INCLUDE_DIR YAMLCPP_LIBRARY)
