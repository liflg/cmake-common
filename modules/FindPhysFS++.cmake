# - Try to find the library PhysicsFS++ (https://github.com/kahowell/physfs-cpp)
# Once done this will define
#  PHYSFS++_FOUND - System has PhysicsFS++
#  PHYSFS++_INCLUDE_DIRS - The PhysFS++ include directories
#  PHYSFS++_LIBRARIES - The libraries needed to use PhysFS++

find_path(PHYSFS++_INCLUDE_DIR physfs.hpp)

find_library(PHYSFS++_LIBRARY NAMES physfs++)

set(PHYSFS++_LIBRARIES ${PHYSFS++_LIBRARY})
set(PHYSFS++_INCLUDE_DIRS ${PHYSFS++_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set PHYSFS++_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(PHYSFS++ DEFAULT_MSG
                                  PHYSFS++_LIBRARY PHYSFS++_INCLUDE_DIR)

mark_as_advanced(PHYSFS++_INCLUDE_DIR PHYSFS++_LIBRARY)
