if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU") #All our games are written in C++, right?
    set(IS_GCC true)
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    set(IS_CLANG true)
else()
    set(IS_GCC false)
    set(IS_CLANG false)
endif()

set(SHARED_FLAGS_DEBUG_INIT "-O0 -ggdb")
set(SHARED_FLAGS_RELWITHDEBINFO_INIT "-O3 -ggdb -DNDEBUG")
set(SHARED_FLAGS_INIT "-Wall -Wextra -Wno-unused -Wno-long-long -Wno-variadic-macros")

#Turn some important warnings into errors
set(${SHARED_FLAGS_INIT} "${SHARED_FLAGS_INIT} -Werror=multichar -Werror=pointer-arith")
#Warn when using uninitialized variables
set(SHARED_FLAGS_INIT "${SHARED_FLAGS_INIT} -Werror=uninitialized ")

#Enable checks which are available for clang and GCC 4.5+ only
if(IS_CLANG OR (IS_GCC AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "4.5"))
    set(SHARED_FLAGS_INIT "${SHARED_FLAGS_INIT} -Werror=conversion-null")
endif()
#Enable checks which are available for Clang and GCC 4.8+ only
if(IS_CLANG OR (IS_GCC AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "4.8"))
    set(SHARED_FLAGS_INIT "${SHARED_FLAGS_INIT} -Werror=sizeof-pointer-memaccess")
endif()

#Enable checks which are available for GCC 4.7+ only
if(IS_GCC AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "4.7")
    set(SHARED_FLAGS_INIT "${SHARED_FLAGS_INIT} -Werror=maybe-uninitialized")
endif()

#Force flags for our pre-defined build types
set(CMAKE_C_FLAGS_DEBUG_INIT "${SHARED_FLAGS_DEBUG_INIT} ${CMAKE_C_FLAGS_DEBUG}")
set(CMAKE_C_FLAGS_RELWITHDEBINFO_INIT "${SHARED_FLAGS_RELWITHDEBINFO_INIT} ${CMAKE_C_FLAGS_RELWITHDEBINFO}")
set(CMAKE_C_FLAGS_INIT "${SHARED_FLAGS_INIT} ${CMAKE_C_FLAGS}")

#And the same for the C++ compiler
set(CMAKE_CXX_FLAGS_DEBUG_INIT "${SHARED_FLAGS_DEBUG_INIT} ${CMAKE_CXX_FLAGS_DEBUG}")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO_INIT "${SHARED_FLAGS_RELWITHDEBINFO_INIT} ${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")
#Enable C++11 support
#Use the flag -std=c++0x for GCC 4.3 to 4.6 and std=c++11 for GCC 4.7+ and clang
if(IS_GCC AND (NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "4.3" AND CMAKE_CXX_COMPILER_VERSION VERSION_LESS "4.7"))
    set(CMAKE_CXX_FLAGS_INIT "${SHARED_FLAGS_INIT} ${CMAKE_CXX_FLAGS} -std=c++0x")
elseif(IS_CLANG OR (IS_GCC AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "4.7"))
    set(CMAKE_CXX_FLAGS_INIT "${SHARED_FLAGS_INIT} ${CMAKE_CXX_FLAGS} -std=c++11")
endif()

#Needed to build C++ code with GCC 5 (or newer) which is compatible with the old ABI.
#FIXME Once we use GCC 5 or newer on our build server, this can be removed.
option(FORCE_GCC_PRE_CXX11_ABI "Force GCC 5+ to use the pre-C++11 ABI" ON)
if(FORCE_GCC_PRE_CXX11_ABI)
  add_definitions(-D_GLIBCXX_USE_CXX11_ABI=0)
endif()

option(CHECK_COMMENT "Check doxygen comments (Clang only)" ON)
if(CHECK_COMMENT AND IS_CLANG)
  add_compile_options(-Wdocumentation)
endif()

#Generate compilation database for usage with clang-tidy, etc.
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
