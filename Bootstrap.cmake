#CMAKE_CURRENT_LIST_DIR is available since 2.8.3
#http://www.cmake.org/Wiki/CMake_Useful_Variables#Locations
cmake_minimum_required(VERSION 2.8.3)

if(DEFINED PROJECT_NAME)
    message(FATAL_ERROR "Do not call project() before including cmake-common.")
endif()

#Exit if someone tries to contaminate the source directory with an in-source build
if(${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
    message (FATAL_ERROR "Please do out-of-source builds.\nCleanup: \"rm -rf CMakeCache.txt CMakeFiles/\"")
endif()

set(CMAKE_COMMON_BASE_DIRECTORY ${CMAKE_CURRENT_LIST_DIR})

#Include our own FindXY.cmake files
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_COMMON_BASE_DIRECTORY}/modules/")
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/modules/external")

include(GetGitRevisionDescription)
get_git_head_revision(GIT_REFSPEC GIT_SHA1)

include(Buildoptions)
include(AddInstallerTarget)
include(AddAnalyzerTarget)
include(AddIwyuSupport)
include(Sanitizers)

#Include pre-defined variables
#Note: Using CMAKE_CURRENT_LIST_DIR as CMAKE_USER_MAKE_RULES_OVERRIDE is bugged in some cmake versions
#      Link: http://public.kitware.com/Bug/view.php?id=11725
set(CMAKE_USER_MAKE_RULES_OVERRIDE "${CMAKE_CURRENT_LIST_DIR}/InitialVariables.cmake")
