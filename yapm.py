import os, shutil

def version():
    print('YAPM 0.1')

def removeTrailingSlash(path):
    return path[:-1] if path[-1] == '/' else path

def copyDir(src, dest, ignoreFiles = []):
    src = removeTrailingSlash(src)
    dest = removeTrailingSlash(dest)
    for src_dir, dirs, files in os.walk(src):
        dst_dir = src_dir.replace(src, dest)
        if not os.path.exists(dst_dir):
            os.mkdir(dst_dir)
        for file_ in files:
            if file_ in ignoreFiles:
                continue
            src_file = os.path.join(src_dir, file_)
            dst_file = os.path.join(dst_dir, file_)
            if os.path.islink(src_file):
                linkto = os.readlink(src_file)
                os.symlink(linkto, dst_file)
            else:
                shutil.copy(src_file, dst_dir)

        for subDir in dirs:
            src_subDir = os.path.join(src_dir, subDir)
            if os.path.islink(src_subDir):
                linkto = os.readlink(src_subDir)
                os.symlink(linkto, os.path.join(dst_dir, subDir))
    return

def createSymLinks(src, dest, filter = []):
    ''' (str, str, list of str) -> NoneType
        Create symbolic links for all files found in src.
        The symbolic links will be created in dest.
        If directories found in src do not exist in dest, they will be created.
        If a file or directory with the same name as the symbolic is found, an OSError is raised.
        If filter is not empty, a symbolic link is only created if the file is in the filter list.
    '''
    src = removeTrailingSlash(src)
    dest = removeTrailingSlash(dest)
    for src_dir, dirs, files in os.walk(src):
        dst_dir = src_dir.replace(src, dest)

        for file_ in files:
            # only check if file_ is in the filter list if there is at least one item in the filter list
            if len(filter) > 0 and file_ not in filter:
                continue
            if not os.path.exists(dst_dir):
                os.makedirs(dst_dir)
            src_file = os.path.join(src_dir, file_)
            dst_file = os.path.join(dst_dir, file_)
            os.symlink(os.path.relpath(src_file, os.path.dirname(dst_file)), dst_file)
